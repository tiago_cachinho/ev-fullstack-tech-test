import React, { useState } from "react";
import ClientList from "./components/ClientList";

const App = () => {
  const headers = { id: "ID", name: "Client Name", address: "Address" };
  const [list, setList] = useState([]);
  const clientListProps = { headers, list, setList };

  return (
    <div>
      <h1>EVPro Full-stack Test</h1>
      <ClientList {...clientListProps} />
    </div>
  );
};

export default App;
