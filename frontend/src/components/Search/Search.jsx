import React, { useRef } from "react";
import { getClients, searchClients } from "../api";

const FilterClients = (searchInput, setList) => {
  const term = searchInput.current.value;
  if (term) {
    searchClients(term).then((response) =>
      setList((state) => (state = response))
    );
  } else {
    getClients().then((response) => setList((state) => (state = response)));
  }
};

const Search = ({ setList }) => {
  const searchInput = useRef();

  return (
    <input
      type="text"
      placeholder="Search..."
      onChange={() => FilterClients(searchInput, setList)}
      ref={searchInput}
    />
  );
};

export default Search;
