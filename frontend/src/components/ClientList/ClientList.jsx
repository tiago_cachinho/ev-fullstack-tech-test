import React, { useEffect } from "react";
import { getClients } from "../api";
import Form from "../Form";
import Search from "../Search";
import Table from "../Table";

const ClientList = ({ headers, list, setList }) => {
  useEffect(() => {
    getClients().then((response) => {
      setList((state) => (state = response));
    });
  }, [getClients]);

  return (
    <>
      <div className="header-list">
        <Form setList={setList} />
        <Search setList={setList} />
      </div>
      <Table headers={headers} list={list} />
    </>
  );
};

export default ClientList;
