import React, { useRef } from "react";
import { getClients, addClient } from "../api";

const createAddClientRequest = (clientInfo, setList) => {
  addClient(clientInfo);
  getClients().then((res) => setList(res));
};

const Form = ({ setList }) => {
  const clientName = useRef();
  const clientAddress = useRef();

  return (
    <form
      onSubmit={(e) => {
        e.preventDefault();
        createAddClientRequest(
          {
            name: clientName.current.value,
            address: clientAddress.current.value,
          },
          setList
        );
        clientName.current.value = "";
        clientAddress.current.value = "";
      }}
    >
      <h4>Add Client</h4>
      <label htmlFor="client-name-input">
        Name:
        <input
          id="client-name-input"
          type="text"
          placeholder="Client name..."
          ref={clientName}
        />
      </label>
      <label htmlFor="client-address-input">
        Address:
        <input
          id="client-address-input"
          type="text"
          placeholder="Client address..."
          ref={clientAddress}
        />
      </label>
      <button type="submit">Add Client</button>
    </form>
  );
};

export default Form;
