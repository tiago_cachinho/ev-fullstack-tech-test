import React from "react";
import { deleteClient } from "../api";

const Table = ({ headers, list }) => (
  <table>
    <thead>
      <tr>
        {Object.values(headers).map((header) => (
          <th key={header}>{header}</th>
        ))}
      </tr>
    </thead>
    <tbody>
      {list.map((item) => (
        <tr key={`client-id-${item?.id}`}>
          {Object.keys(headers).map((cell) => (
            <td key={`client-${cell.toLowerCase().replace(' ', '-')}-${item?.id}}`}>
              {item?.[cell.toLowerCase()]}
            </td>
          ))}
          <td><button onClick={() => deleteClient(item?.id)}>Delete</button></td>
        </tr>
      ))}
    </tbody>
  </table>
);

export default Table;
