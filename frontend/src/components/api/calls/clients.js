import invariant from "invariant";
import { api, clientUrl } from "..";

export async function getClients() {
  const response = await api.get(clientUrl, {
    method: "GET",
  });
  invariant(response.data, "Could not retrieve clients list");
  return response.data;
}

export async function addClient(client) {
  const response = await api.post(clientUrl, client);
  return response.data;
}

export async function searchClients(term) {
  const response = await api.get(`${clientUrl}/${term}`);
  return response.data;
}

export async function deleteClient(id) {
  const response = await api.delete(`${clientUrl}/${id}`);
  return response.data;
}
