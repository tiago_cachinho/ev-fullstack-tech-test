import axios from "axios";
import { v4 } from "uuid";

const requestId = v4();

axios.defaults.timeout = 600000;
axios.defaults.headers.common["request-id"] = requestId;

const api = axios.create({
  responseTypes: "json",
});

export default api;
