import api from "./api";
import { getClients, addClient, searchClients, deleteClient } from "./calls/clients";

const apiDomain = "http://localhost:3001/";
const clientUrl = apiDomain + "clients/";

export { api, getClients, addClient, searchClients, deleteClient, clientUrl };
