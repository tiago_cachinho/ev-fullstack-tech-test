import "regenerator-runtime/runtime";
import {
  getClients,
  addClient,
  searchClients,
  deleteClient,
} from "../../src/components/api";

test("Get Client List", () => {
  let list = [];
  getClients().then((res) => {
    list = res;
    expect(list).toEqual(expect.arrayContaining([{}]));
  });
});

test("Add Client to List", () => {
  const newEntry = { id: "1", name: "Test", address: "Subject" };
  let list = [];
  addClient(newEntry).then((res) => {
    expect(res).toBe({
      status: "success",
      message: "Client added successfully.",
    });
    getClients().then((res) => {
      list = res;
      expect(list).toEqual(expect.arrayContaining([newEntry]));
    });
  });
});

test("Search Client List", () => {
  let list = [];
  searchClients().then((res) => {
    list = res;
    expect(list.length).toEqual(2);
  });

  searchClients("ti").then((res) => {
    list = res;
    expect(list).toContainObject({ name: "Tiago Cachinho" });
  });
});

test("Delete Client from List", () => {
  const newEntry = { id: "1", name: "Test", address: "Subject" };
  let list = [];
  addClient(newEntry);
  deleteClient("1").then((res) => {
    list = res;
    expect(list.length).toEqual(2);
  });
});
