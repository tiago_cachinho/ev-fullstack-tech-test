const fuzzySearch = require("../../src/utils/FuzzySearch");

test('Fuzzy Search', () => {
  const data = [{a: 'Easy', b: 'Test'}, {a: 'Hard', b: 'Test'}];
  expect(fuzzySearch(data, 'Ea').length).toEqual(1);
  expect(fuzzySearch(data, 'T').length).toBe(2);
});