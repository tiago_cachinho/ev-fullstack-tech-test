const fuzzySearch = (items, query) => {
  const search = query.split(" ");
  const found = [];

  items.forEach((i) => {
    let matches = 0;
    search.forEach((s) => {
      let props = 0;
      for (var prop in i) {
        if (i[prop]?.toString().toLowerCase().indexOf(s.toLowerCase()) > -1) {
          props++;
        }
      }
      if (props >= 1) {
        matches++;
      }
    });
    if (matches == search.length) {
      found.push(i);
    }
  });
  return found;
}

module.exports = fuzzySearch;
