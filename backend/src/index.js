const express = require("express");
const bodyParser = require("body-parser");
const cors = require('./cors');

const clientRoutes = require("./routes/clients.js");

const app = express();
const PORT = 3001;

app.use(cors);
app.use(bodyParser.json());
app.use("/clients", clientRoutes);

app.get("/", (req, res) => res.send("<h1>API Running.</h1>"));

app.listen(PORT, () => {
  console.log(`Server started on port ${PORT}`);
});
