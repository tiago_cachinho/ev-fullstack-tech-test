const uuid = require("uuid");
const fuzzySearch = require("../utils/FuzzySearch");
const uuidv4 = uuid.v4;

const clients = [
  {
    id: uuidv4(),
    name: "Tiago Cachinho",
    address: "Somewhere",
  },
  {
    id: uuidv4(),
    name: "Marina Cachinho",
    address: "Nowhere",
  },
];

const listClients = (req, res) => {
  res.send(clients);
};

const addClient = (req, res) => {
  try {
    const client = req.body;
    clients.push({ ...client, id: uuidv4() });
    res.send('{"status": "success", "message": "Client added successfully."}');
  } catch (e) {
    console.error(e);
    res.send('{"status": "error", "message": "Failed to add client."}');
  }
};

const searchClients = (req, res) => {
  const { term } = req.params;
  res.send(fuzzySearch(clients, term));
};

const deleteClient = (req, res) => {
  const tempClients = clients.splice(0, clients.length);
  try {
    const { id } = req.params;

    clients.push(...tempClients.filter((client) => client.id !== id));
    res.send(
      '{"status": "success", "message": "Client deleted successfully."}'
    );
  } catch (e) {
    clients.push(...tempClients);
    console.error(e);
    res.send('{"status": "error", "message": "Could not delete client."}');
  }
};

const clientController = {
  listClients,
  addClient,
  searchClients,
  deleteClient,
};

module.exports = clientController;
