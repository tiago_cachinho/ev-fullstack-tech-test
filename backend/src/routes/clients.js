const express = require("express");
const { listClients, addClient, searchClients, deleteClient } = require("../controllers/clients.js");

const router = express.Router();

// List all clients
router.get("/", listClients);

// Add client
router.post("/", addClient);

// Search client
router.get("/:term", searchClients);

// Delete client
router.delete("/:id", deleteClient);

module.exports = router;
